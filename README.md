# docker_env_test
Build Status : [![build status](https://gitlab.com/dgoo2308/docker_env_test/badges/master/build.svg)](https://gitlab.com/dgoo2308/docker_env_test/commits/master)
## Purpose
Test case for question on Stack overflow answer:
[Set ENV variable in container is not working, is every under "/usr/local/bin" executed on container run?](http://stackoverflow.com/questions/39670894/set-env-variable-in-container-is-not-working-is-every-under-usr-local-bin-ex/39716204#39716204)
## Test
in the Dockerfile we define `ENV ENV_TEST=Dockerfile`

in container-files/setup.sh : `echo $ENV_TEST`

Thus running the container should give us `Dockerfile` or `overide`


## Tests
Check out the Build to see test results
(GitLab-CI), all it takes is a .gitlab-ci.yml file

## test local:

```
git clone https://gitlab.com/dgoo2308/docker_env_test.git
cd docker_env_test
```

Build de container:
```
docker build -t env_test .
```

run test it using Dockerfile ENV for `ENV_TEST`

```
docker run env_test
```

Or overide ENV_TEST with the new value: 'overide'


```
docker run ENV_TEST='overide' env_test
```

Run Test and have shell:

```
docker run -it env_test
```
or
```
docker run -it -e ENV_TEST='overide' env_test
```

## Production

run docker in background

```
$MYCONTAINER=`docker run  -d -it env_test`
```
and `docker attach $MYCONTAINER` (might have to press a key to get the prompt of your container)
and `docker stop $MYCONTAINER`

